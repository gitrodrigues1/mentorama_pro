package com.mentorama.springweb;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/messages")
public class MessageController {

    private final List<Message> messages;

    public MessageController() {
        this.messages = new ArrayList<>();
    }

    @GetMapping
    public List<Message> findAll(@RequestParam(required = false) String message) {
        if (message != null) {
            return messages.stream()
                    .filter(msg -> msg.getMessage().contains(message))
                    .collect(Collectors.toList());
        }
        return messages;
    }

    @GetMapping("/{id}")
    public Message findById(@PathVariable Integer id) {
        return messages.stream()
                .filter(msg -> msg.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @PostMapping
    public Integer add(@RequestBody final Message message) {
        if(message.getId() == null) {
            message.setId(messages.size() + 1);
        }
        messages.add(message);
        return message.getId();
    }

    @PutMapping
    public void update(@RequestBody final Message message) {
        messages.stream()
                .filter(msg -> msg.getId().equals(message.getId()))
                .forEach(msg -> msg.setMessage(message.getMessage()));
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        messages.removeIf(msg -> msg.getId().equals(id));
    }
}
